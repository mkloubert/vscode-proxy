# Change Log (vscode-proxy)

## 0.17.1 (October 14th, 2017; multi root support)

* started to refactor to new, upcoming [Multi Root Workspace API](https://github.com/Microsoft/vscode/wiki/Extension-Authoring:-Adopting-Multi-Root-Workspace-APIs)

## 0.16.1 (September 7th, 2017)

* added `destination` property to [ChunkHandlerModuleExecutorArguments](https://mkloubert.github.io/vscode-proxy/interfaces/_contracts_.chunkhandlermoduleexecutorarguments.html) interface

## 0.15.1 (September 4th, 2017)

* first beta release
